import { ProductType } from './ProductType';

export type ServerOrderType = {
  id: number,
  first_name: string,
  last_name: string,
  phone_number: string,
  email: string,
  adress: string,
  order_time: string,
  solved: boolean,
  price: string,
  product_infos: ServerOrderProductInfo[],
};

export type ServerOrderProductInfo = {
  quantity: number,
  product: ProductType
};

export type OrderType = {
  id: number,
  firstName: string,
  lastName: string,
  phoneNumber: string,
  email: string,
  adress: string,
  orderTime: string,
  solved: boolean,
  price: string,
  productInfos: OrderProductInfo[],
};

export type OrderProductInfo = {
  quantity: number,
  product: ProductType
}

export type OrderFieldsType = 'firstName' | 'lastName' | 'phoneNumber' | 'email' | 'adress';
