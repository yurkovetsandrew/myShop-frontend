export type ServerProductType = {
  title: string,
  description: string
  price: string,
  oldPrice: string,
  image: string,
  gender: string,
  id: string
};

export type ProductType = {
  title: string,
  description: string
  price: string,
  oldPrice: string,
  image: string,
  gender: string,
  id: string
};

export type ProductFieldsType = 'title' | 'description' | 'price' | 'oldPrice' | 'image' | 'gender';
