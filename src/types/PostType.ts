export type ServerPostType = {
  title: string,
  body: string,
  id: number,
  image: string
};

export type PostType = {
  title: string,
  body: string,
  id: number,
  image: string
};

export type PostFieldsType = 'title' | 'body' | 'image';
