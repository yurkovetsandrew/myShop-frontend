declare var DEV_MODE: boolean;

const JSON_HEADER = 'application/json';

export default class BaseApi {
  authFetch: (uri: RequestInfo, options?: RequestInit) => Promise<Response>;

  static HOST_URL = '/';

  constructor( fetch: (uri: RequestInfo, options?: RequestInit) => Promise<Response> ) {
    this.authFetch = fetch;
  }

  getData( url: string, additionalHeaders?: HeadersInit ) {
    return this.authFetch( url, {
      ...this.getRequestParams( 'GET', additionalHeaders ),
    } ).then( this.processResponse );
  };

  postData = ( url: string, data: any, additionalHeaders?: HeadersInit ): Promise<any> => {
    return this.authFetch( url, {
      ...this.getRequestParams( 'POST', additionalHeaders ),
      body: JSON.stringify( data )
    } ).then( this.processResponse );
  };

  putData = ( url: string, data: any, additionalHeaders?: HeadersInit ): Promise<any> => {
    return this.authFetch( url, {
      ...this.getRequestParams( 'PUT', additionalHeaders ),
      body: JSON.stringify( data )
    } ).then( this.processResponse );
  };

  deleteData = ( url: string, data: any = {}, additionalHeaders?: HeadersInit ): Promise<any> => {
    return this.authFetch( url, {
      ...this.getRequestParams( 'DELETE', additionalHeaders ),
      body: JSON.stringify( data )
    } ).then( this.processResponse );
  };

  getRequestParams = ( method: string, additionalHeaders: HeadersInit = {} ) => {
    return {
      credentials: 'include',
      headers: {
        'Accept': JSON_HEADER,
        'Content-Type': JSON_HEADER,
        ...additionalHeaders
      },
      method,
    } as RequestInit;
  };

  processResponse = ( response: Response ) => {
    return response.json().then( ( res ) => {
      if (!response.ok) {
        throw res;
      }
      return res;
    } );
  }
}
