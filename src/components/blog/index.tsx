import React from 'react';
import Post from './post';
import { PostType } from '../../types/PostType'
import './styles.scss';

interface IPostsState {
  posts: PostType[];
}

class Blog extends React.PureComponent<{}, IPostsState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      posts: []
    }
  }

  componentDidMount() {
    this.fetchPosts()
  }

  fetchPosts() {
    fetch(`http://localhost:3000/api/articles`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        posts: response,
      })
    })
  }

  render() {
    const posts = this.state.posts

    return (
      <div className='pt-15 mh-832'>
        <h1 className='text-center py-5'>BLOG</h1>
        <div className='d-flex align-content-start flex-wrap justify-content-center mx-5'>
          {posts.length ? posts.map( (post, index) => (
            <Post key = {index} post = {post} />
          )) : (
            <div className='text-center my-5 product-not-found'>
              <h1>Posts not found</h1>
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default Blog;
