import React from 'react';
import Links from '../home/links';
import { PostType } from '../../types/PostType'
import './styles.scss';

interface IPostsState {
  post: PostType;
}

class PostPage extends React.PureComponent<{}, IPostsState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      post: {
        title: '',
        body: '',
        image: '',
        id: null,
      },
    }
  }

  componentDidMount() {
    const id = +decodeURI(window.location.pathname).match(/.*\/post\/((\w|[^\x00-\x7F]|-)+)/)[1]
    fetch(`http://localhost:3000/api/articles/${id}`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        post: response,
      })
    })
  }

  render() {
    const post = this.state.post
    const divStyle = {
      color: 'blue',
      backgroundImage: 'url(' + post.image + ')',
    }

    return (
      <div className='mx-5 mh-832 post-page pt-15 px-5'>
        <h1 className='mt-5 text-center'>{post.title}</h1>
        <h4 className='mt-5 text-center'>{post.body}</h4>
        <hr className='hr-post'/>
        <div className='mt-5 d-flex justify-content-center'>
          <div className='post-image post-image-big mx-5' style={divStyle}></div>
        </div>
        <h1 className='text-center my-5'>FOLLOW US</h1>
        <Links />
      </div>
    )
  }
}

export default PostPage;
