import React from 'react';
import { PostType } from '../../types/PostType';
import './styles.scss';

interface IProductProps {
  post: PostType;
}

class Post extends React.PureComponent<IProductProps> {
  constructor(props: IProductProps) {
    super(props);
  }

  render() {
    const post = this.props.post
    const divStyle = {
      color: 'blue',
      backgroundImage: 'url(' + post.image + ')',
    }

    return (
      <div className='post-card post-image text-center mx-5 mb-5' style={divStyle}>
        <a href={`/post/${post.id}`}>
          <div className='mx-auto mb-2 my-auto d-flex flex-column align-items-center justify-content-center'>
            <h3 className='mw-100'>{post.title}</h3>
            <button className='btn mt-5'>Read</button>
          </div>
        </a>
      </div>
    )
  }
}

export default Post;
