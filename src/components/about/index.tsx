import React from 'react';
import './styles.scss';

class About extends React.PureComponent {

  componentDidMount() {
    document.querySelector('.navbar').classList.remove('only-on-home-page')
  }

  render() {
    return (
      <div className='pt-15'>
        <h1 className='mt-5 text-center'>Nature store story</h1>
        <div className='px-15 mb-5'>
          <div className='d-flex pt-5 mb-5'>
            <div className='about-image about-1 w-100'></div>
            <p className='px-5 my-auto about-text'>
              The <b>Nature store</b> is a company based in Minsk, Belarus.<br/>  Founder and visionary for our company, Andrew, grew up in a family that understood the importance of natural, plant-based, and eco-friendly products. His childhood is filled with memories of his dad making natural things out of simple yet effective ingredients that were safe for the whole family to use.<br/>  As a child and young adult, Andrew suffered from eczema and dry skin. Instead of using steroids or harmful creams, his parents made soothing products with natural ingredients that helped to keep his skin protected for the long term.<br/>After moving from his home in Petrikov to Minsk, he found himself out of luck when it came to the natural, homemade products he was used to and so, he began his things adventures and experimentations. Armed knowledge passed down from his parents, <b>Nature store</b> was born.
            </p>
          </div>
          <div className='d-flex'>
            <p className='px-5 my-auto about-text'>
              Combining his passion for natural ingredients and his expert scientific knowledge, Andrew built his business up with a steady base of friends who loved her products and spread the word to make <b>Nature store</b> what it is today.<br/>  Protecting a healthy environment, offering natural alternatives, and creating a positive impact are at the core of everything that we do. Natural and plant-based products are better for your health, your home, and the world as a whole.<br/> For honest products using pure ingredients, try <b>Nature store</b> today.
            </p>
            <div className='about-image about-2 w-100'></div>
          </div>
        </div>
      </div>
    )
  }
}

export default About;
