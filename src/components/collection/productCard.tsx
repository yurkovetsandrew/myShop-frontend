import React from 'react';
import { ProductType } from '../../types/ProductType';
import './styles.scss';

interface IProductProps {
  product: ProductType;
}

class ProductCard extends React.PureComponent<IProductProps> {
  constructor(props: IProductProps) {
    super(props);
  }

  render() {
    const product = this.props.product
    const divStyle = {
      color: 'blue',
      backgroundImage: 'url(' + product.image + ')',
    }

    return (
      <div className='jumbotron product-card text-center mx-5 mb-5'>
        <a href={`/product/${product.id}`}>
          <div className='product-image mx-auto mb-3' style={divStyle}></div>
          <h6>{product.title}</h6>
          {product.oldPrice ? (
            <h4>
              <del className='old-price mr-3'>{product.oldPrice}</del>
            {'   ' + `${product.price}` + '$'}</h4>
          ) : (
            <h4>{`${product.price}` + '$'}</h4>
          )}
        </a>
      </div>
    )
  }
}

export default ProductCard;
