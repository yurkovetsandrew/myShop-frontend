import React from 'react';
import ProductCard from './productCard';
import { ProductType } from '../../types/ProductType'
import PostCard from '../blog/post';
import { PostType } from '../../types/PostType';
import './styles.scss';

interface ICollectinProps {
  gender: string;
}

interface ICollectionState {
  allProducts: ProductType[],
  filterProducts: ProductType[],
  posts: PostType[]
}

class Collection extends React.PureComponent<ICollectinProps, ICollectionState> {
  constructor(props: ICollectinProps) {
    super(props);
    this.state = {
      allProducts: [],
      filterProducts: [],
      posts: []
    }
  }

  componentDidMount() {
    this.fetchProducts()
    this.fetchPosts()
  }

  fetchProducts() {
    fetch(`http://localhost:3000/api/products`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        allProducts: response,
      })
    })
  }

  fetchPosts() {
    fetch(`http://localhost:3000/api/articles?limit=3`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        posts: response,
      })
    })
  }

  render() {
    const { allProducts, posts } = this.state
    const productsByGender = allProducts.filter( product => product.gender == this.props.gender)

    return (
      <div className='pt-15 collection'>
        <h1 className='text-center my-5'>{this.props.gender == 'man' ? 'Men things' : 'Women things'}</h1>
        <div className='d-flex align-content-start flex-wrap justify-content-center mx-5 mb-5'>
          {productsByGender.length ? productsByGender.map( (product, index) => (
            <ProductCard key = {index} product = {product} />
          )) : (
            <div className='text-center my-5 product-not-found'>
              <h1>Products not found</h1>
            </div>
          )}
        </div>
        <hr/>
        <div className='last-posts'>
          <h2 className='text-center mb-5 mt-3'>New Posts</h2>
          <div className='d-flex justify-content-between'>
            {posts.map( (post, index) => (
              <div>
                <PostCard key = {index} post = {post} />
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}

export default Collection;
