import React from 'react';
import { ProductType } from '../../types/ProductType';
import ProductCard from '../collection/productCard';
import PostCard from '../blog/post';
import { PostType } from '../../types/PostType';
import './styles.scss';

interface IProductState {
  product: ProductType,
  addedToCart: boolean,
  products: ProductType[],
  posts: PostType[]
}

class ProductPage extends React.PureComponent<{}, IProductState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      product: {
        title: '',
        description: '',
        image: '',
        price: '',
        oldPrice: '',
        gender: '',
        id: ''
      },
      addedToCart: false,
      products: [],
      posts: []
    }
  }

  componentDidMount() {
    const id = +decodeURI(window.location.pathname).match(/.*\/product\/((\w|[^\x00-\x7F]|-)+)/)[1]
    fetch(`http://localhost:3000/api/products/${id}`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        product: response,
      })
      let productsInCart:ProductType[] = JSON.parse(localStorage.getItem('productsInCart'))
      productsInCart.map( (product:ProductType) => {
        if(product.id === this.state.product.id) {
          this.setState({
            addedToCart: true
          })
        }
      })
      this.fetchProducts()
      this.fetchPosts()
    })
  }

  fetchProducts() {
    fetch(`http://localhost:3000/api/products?limit=4`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        products: response,
      })
    })
  }

  fetchPosts() {
    fetch(`http://localhost:3000/api/articles?limit=3`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        posts: response,
      })
    })
  }

  addToCart = () => {
    let productsInCart:ProductType[] = []
    if(JSON.parse(localStorage.getItem('productsInCart')) !== null) {
      productsInCart = JSON.parse(localStorage.getItem('productsInCart'))
    }
    productsInCart.push(this.state.product)
    localStorage.setItem('productsInCart', JSON.stringify(productsInCart))
    this.setState({
      addedToCart: true
    })
  }

  render() {
    const { product, addedToCart, products, posts } = this.state;
    const divStyle = {
      color: 'blue',
      backgroundImage: 'url(' + product.image + ')',
    }

    return (
      <div className='mx-5 product-page pt-15 px-5'>
        <div className='d-flex'>
          <div className='product-image my-5 mx-5' style={divStyle}></div>
          <div className='product-body mx-5 p-5 w-50'>
            <h1 className='mt-5'>{product.title}</h1>
            {product.oldPrice ? (
              <h4 className='product-price'>
                <del className='old-price mr-3'>{product.oldPrice}</del>
              {'  ' + `${product.price}` + '$'}</h4>
            ) : ('')}
            <p className='mt-3'>{product.description}</p>
            <button className='add-to-cart-button btn btn-outline-secondary mt-5 w-50' onClick={this.addToCart} disabled={addedToCart}>
              Add to cart
            </button>
          </div>
        </div>
        <div className='new-arrivals'>
          <h2 className='text-center mb-5 mt-3'>New Arrivals</h2>
          <div className='d-flex justify-content-between'>
            {products.map( (productFormNewArrivals, index) => (
              <div>
                <ProductCard key = {index} product = {productFormNewArrivals} />
              </div>
            ))}
          </div>
        </div>
        <hr/>
        <div className='last-posts'>
          <h2 className='text-center mb-5 mt-3'>New Posts</h2>
          <div className='d-flex justify-content-between'>
            {posts.map( (post, index) => (
              <div>
                <PostCard key = {index} post = {post} />
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}

export default ProductPage;
