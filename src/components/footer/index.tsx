import React from 'react';
import './styles.scss';

class Footer extends React.PureComponent {

  render() {
    return (
      <div className='h-10'>
        <hr/>
        <p className='footer-text text-center mt-4'>© 2021 Nature store. All Rights Reserved.</p>
      </div>
    )
  }
}

export default Footer;
