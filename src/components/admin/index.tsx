import React from 'react';
import Products from './products';
import Orders from './orders';
import Posts from './posts';
import './styles.scss';

class Admin extends React.PureComponent {

  componentDidMount() {
    if(localStorage.natureAdmin !== 'true') {
      window.location.href = 'http://localhost:3000/account'
    }

    const collaps = document.querySelectorAll('.collapsible')
    collaps.forEach( (elem) => {
      elem.addEventListener('click', () => {
        elem.classList.toggle('active')
        let nextElem:any = elem.nextElementSibling
        if(nextElem) {
          if (nextElem.style.maxHeight){
            nextElem.style.maxHeight = null
          } else {
            nextElem.style.maxHeight = nextElem.scrollHeight + "px"
          }
        }
      })
    })
  }

  // componentDidUpdate() {
  //   const collaps = document.querySelectorAll('.collapsible')
  //   collaps.forEach( (elem) => {
//       if(elem.classList.contains('active')) {
//         let nextElem:any = elem.nextElementSibling
//         nextElem.style.maxHeight = nextElem.scrollHeight + "px"
//       }
  //   })
  // }

  logOut = () => {
    localStorage.natureAdmin = 'false'
    window.location.href = 'http://localhost:3000/account'
  }

  render() {

    return (
      <div className='pt-15 mh-832 px-2'>
        <div className='d-flex justify-content-end'>
          <button type="button" className="btn btn-light" onClick={this.logOut}>Log out</button>
        </div>
        <div className='d-flex justify-content-center'>
          <h2 className='my-4'>Dashboard</h2>
        </div>
        <div className='w-100'>
          <div className='jumbotron admin-jumbotron'>
            <div className='collapsible d-flex justify-content-between'>
              <h5>Products</h5>
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-caret-down" viewBox="0 0 16 16">
                <path d="M3.204 5h9.592L8 10.481 3.204 5zm-.753.659 4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
              </svg>
            </div>
            <Products />
          </div>
          <div className='jumbotron admin-jumbotron mb-2'>
            <div className='collapsible d-flex justify-content-between'>
              <h5>Orders</h5>
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-caret-down" viewBox="0 0 16 16">
                <path d="M3.204 5h9.592L8 10.481 3.204 5zm-.753.659 4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
              </svg>
            </div>
            <Orders />
          </div>
          <div className='jumbotron admin-jumbotron mb-2'>
            <div className='collapsible d-flex justify-content-between'>
              <h5>Posts</h5>
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-caret-down" viewBox="0 0 16 16">
                <path d="M3.204 5h9.592L8 10.481 3.204 5zm-.753.659 4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
              </svg>
            </div>
            <Posts />
          </div>
        </div>
      </div>
    )
  }
}

export default Admin;
