import React from 'react';
import { ProductType, ProductFieldsType } from '../../types/ProductType';
import './styles.scss';

interface IProductsState {
  products: ProductType[],
  addProduct: boolean,
  saveProductDisabled: boolean,
  newOrEditProduct: ProductType
}

class Product extends React.PureComponent<{}, IProductsState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      products: [],
      addProduct: false,
      saveProductDisabled: true,
      newOrEditProduct: {
        title: '',
        description: '',
        price: '',
        oldPrice: '',
        image: '',
        gender: 'woman',
        id: ''
      }
    }
  }

  componentDidMount() {
    this.fetchProducts()
  }

  fetchProducts() {
    fetch(`http://localhost:3000/api/products`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        products: response,
      })
    })
  }

  addingProduct() {
    this.setState({
      addProduct: true,
    })
  }

  editProduct(product:ProductType) {
    this.setState({
      newOrEditProduct: product,
      addProduct: false,
    })
  }

  saveEditProduct = () => {
    const productId = this.state.newOrEditProduct.id
    fetch(`http://localhost:3000/api/products/${productId}`, {
      method: "put",
      body: JSON.stringify(this.state.newOrEditProduct),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(!response.status) {
        const products = this.state.products.map( product => {
          if(product.id === productId) {
            return response
          } else {
            return product
          }
        })
        this.setState({
          products: products,
          saveProductDisabled: true
        })
        this.clearProductForm()
      }
    })
  }

  deleteProduct(productId:string) {
    fetch(`http://localhost:3000/api/products/${productId}`, { method: 'delete' })
      .then(() => {
        this.fetchProducts()
      })
  }

  saveNewProduct = () => {
    fetch(`http://localhost:3000/api/products`, {
      method: "post",
      body: JSON.stringify(this.state.newOrEditProduct),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(!response.status) {
        this.fetchProducts()
        this.setState({
          saveProductDisabled: true
        })
        this.clearProductForm()
      }
    })
  }

  clearProductForm() {
    this.setState({
      newOrEditProduct: {
        title: '',
        description: '',
        price: '',
        oldPrice: '',
        image: '',
        gender: 'woman',
        id: ''
      },
      addProduct: false,
    })
  }

  changeProductField(field: ProductFieldsType, value:string) {
    const newOrEditProduct:ProductType = Object.assign({}, this.state.newOrEditProduct)
    newOrEditProduct[field] = value
    this.setState({
      newOrEditProduct,
      saveProductDisabled: ['title', 'price'].some( (f: ProductFieldsType) => newOrEditProduct[f] === '' ),
    })
  }

  divStyle = (imageLink:string) => {
    return {
      color: 'blue',
      backgroundImage: 'url(' + imageLink + ')',
    }
  }

  render() {
    const { products, addProduct, saveProductDisabled, newOrEditProduct } = this.state

    return (
      <div className="content">
        <table className='table table-striped table-bordered'>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Title</th>
              <th scope='col'>Description</th>
              <th className='td-gender' scope='col'>Gender</th>
              <th scope='col'>ID</th>
              <th className='td-width' scope='col'>Price</th>
              <th className='td-width' scope='col'>Old price</th>
              <th scope='col'>Image</th>
              <th scope='col' colSpan={2}></th>
            </tr>
          </thead>
          <tbody>
            {products.map( (product, index) => ( newOrEditProduct.id !== product.id ? (
              <tr key = {index}>
                <td>{index + 1}</td>
                <td><b>{product.title}</b></td>
                <td>{product.description}</td>
                <td>{product.gender}</td>
                <td>{product.id}</td>
                <td>{product.price}</td>
                <td>{product.oldPrice}</td>
                <td className='d-flex justify-content-center'>
                  <div className='image-table' style={this.divStyle(product.image)}></div>
                </td>
                <td>
                  <button className='btn btn-outline-warning' onClick={() => this.editProduct(product)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil" viewBox="0 0 16 16">
                      <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                    </svg>
                  </button>
                </td>
                <td>
                  <button className='btn btn-outline-danger' onClick={() => this.deleteProduct(product.id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                      <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                      <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            ) : (
              <tr key = {index}>
                <td>{index + 1}</td>
                <td>
                  <input className="form-control" placeholder="Enter title"
                         required onChange={(e) => this.changeProductField('title', e.target.value)}
                         value={newOrEditProduct.title} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter description"
                         required onChange={(e) => this.changeProductField('description', e.target.value)}
                         value={newOrEditProduct.description} />
                </td>
                <td>
                  <select className="form-select" onChange={(e) => this.changeProductField('gender', e.target.value)}
                          value={newOrEditProduct.gender}>
                    <option value="woman">Woman</option>
                    <option value="man">Man</option>
                  </select>
                </td>
                <td>
                  {newOrEditProduct.id}
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter price"
                         required onChange={(e) => this.changeProductField('price', e.target.value)}
                         value={newOrEditProduct.price} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter oldPrice"
                         required onChange={(e) => this.changeProductField('oldPrice', e.target.value)}
                         value={newOrEditProduct.oldPrice} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter image link"
                         required onChange={(e) => this.changeProductField('image', e.target.value)}
                         value={newOrEditProduct.image} />
                </td>
                <td>
                  <button className='btn btn-outline-success' disabled={saveProductDisabled} onClick={this.saveEditProduct}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
                      <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                    </svg>
                  </button>
                </td>
                <td>
                  <button className='btn btn-outline-dark' onClick={() => this.clearProductForm()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                      <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            )))}
            {addProduct ? (
              <tr>
                <td>{products.length + 1}</td>
                <td>
                  <input className="form-control" placeholder="Enter title"
                         required onChange={(e) => this.changeProductField('title', e.target.value)}
                         value={newOrEditProduct.title} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter description"
                         required onChange={(e) => this.changeProductField('description', e.target.value)}
                         value={newOrEditProduct.description} />
                </td>
                <td>
                  <select className="form-select"
                          onChange={(e) => this.changeProductField('gender', e.target.value)}
                          value={newOrEditProduct.gender}>
                    <option value="woman">Woman</option>
                    <option value="man">Man</option>
                  </select>
                </td>
                <td>
                  {products.length ? (+products[products.length - 1].id + 1) : (0)}
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter price"
                         required onChange={(e) => this.changeProductField('price', e.target.value)}
                         value={newOrEditProduct.price} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter oldPrice"
                         required onChange={(e) => this.changeProductField('oldPrice', e.target.value)}
                         value={newOrEditProduct.oldPrice} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter image link"
                         required onChange={(e) => this.changeProductField('image', e.target.value)}
                         value={newOrEditProduct.image} />
                </td>
                <td>
                  <button className='btn btn-outline-success' disabled={saveProductDisabled} onClick={this.saveNewProduct}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
                      <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                    </svg>
                  </button>
                </td>
                <td>
                  <button className='btn btn-outline-dark' onClick={() => this.clearProductForm()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                      <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            ) : ('')}
          </tbody>
        </table>
        <div className="d-flex flex-row-reverse bd-highlight">
          <button className='btn btn-outline-secondary add' onClick={() => this.addingProduct()} disabled={addProduct}>+ product</button>
        </div>
      </div>
    )
  }
}

export default Product;
