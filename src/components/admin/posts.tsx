import React from 'react';
import { PostType, PostFieldsType } from '../../types/PostType';
import './styles.scss';

interface IPostsState {
  posts: PostType[],
  addPost: boolean,
  savePostDisabled: boolean,
  newOrEditPost: PostType
}

class Posts extends React.PureComponent<{}, IPostsState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      posts: [],
      addPost: false,
      savePostDisabled: true,
      newOrEditPost: {
        title: '',
        body: '',
        image: '',
        id: null
      }
    }
  }

  componentDidMount() {
    this.fetchPosts()
  }

  fetchPosts() {
    fetch(`http://localhost:3000/api/articles`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        posts: response,
      })
    })
  }

  addingPost() {
    this.setState({
      addPost: true,
    })
  }

  editPosts(post:PostType) {
    this.setState({
      newOrEditPost: post,
      addPost: false,
    })
  }

  saveEditPost = () => {
    const postId = this.state.newOrEditPost.id
    fetch(`http://localhost:3000/api/articles/${postId}`, {
      method: "put",
      body: JSON.stringify(this.state.newOrEditPost),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(!response.status) {
        const posts = this.state.posts.map( post => {
          if(post.id === postId) {
            return response
          } else {
            return post
          }
        })
        this.setState({
          posts: posts,
          savePostDisabled: true
        })
        this.clearPostForm()
      }
    })
  }

  deletePost(postId:number) {
    fetch(`http://localhost:3000/api/articles/${postId}`, { method: 'delete' })
      .then(() => {
        this.fetchPosts()
      })
  }

  saveNewPost = () => {
    fetch(`http://localhost:3000/api/articles`, {
      method: "post",
      body: JSON.stringify(this.state.newOrEditPost),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(!response.status) {
        this.fetchPosts()
        this.setState({
          savePostDisabled: true
        })
        this.clearPostForm()
      }
    })
  }

  clearPostForm() {
    this.setState({
      newOrEditPost: {
        title: '',
        body: '',
        image: '',
        id: null
      },
      addPost: false,
    })
  }

  changePostField(field: PostFieldsType, value:string) {
    const newOrEditPost:PostType = Object.assign({}, this.state.newOrEditPost)
    newOrEditPost[field] = value
    this.setState({
      newOrEditPost,
      savePostDisabled: ['title', 'body'].some( (f: 'title'|'body') => newOrEditPost[f] === '' ),
    })
  }

  divStyle = (imageLink:string) => {
    return {
      color: 'blue',
      backgroundImage: 'url(' + imageLink + ')',
    }
  }

  render() {
    const { posts, addPost, savePostDisabled, newOrEditPost } = this.state

    return (
      <div className="content">
        <table className='table table-striped table-bordered'>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Title</th>
              <th scope='col'>Body</th>
              <th scope='col'>Image</th>
              <th scope='col' colSpan={2}></th>
            </tr>
          </thead>
          <tbody>
            {posts.map( (post, index) => ( newOrEditPost.id !== post.id ? (
              <tr key = {index}>
                <td>{index + 1}</td>
                <td>{post.title}</td>
                <td className='w-75'>{post.body}</td>
                <td className='d-flex justify-content-center'>
                  <div className='image-table' style={this.divStyle(post.image)}></div>
                </td>
                <td>
                  <button className='btn btn-outline-warning' onClick={() => this.editPosts(post)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil" viewBox="0 0 16 16">
                      <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                    </svg>
                  </button>
                </td>
                <td>
                  <button className='btn btn-outline-danger' onClick={() => this.deletePost(post.id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                      <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                      <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            ) : (
              <tr key={index}>
                <td>{post.id}</td>
                <td>
                  <input className="form-control" placeholder="Enter title"
                         required onChange={(e) => this.changePostField('title', e.target.value)}
                         value={newOrEditPost.title} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter body"
                         required onChange={(e) => this.changePostField('body', e.target.value)}
                         value={newOrEditPost.body} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter image link"
                         required onChange={(e) => this.changePostField('image', e.target.value)}
                         value={newOrEditPost.image} />
                </td>
                <td>
                  <button className='btn btn-outline-success' disabled={savePostDisabled} onClick={this.saveEditPost}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
                      <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                    </svg>
                  </button>
                </td>
                <td>
                  <button className='btn btn-outline-dark' onClick={() => this.clearPostForm()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                      <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            )))}
            {addPost ? (
              <tr>
                <td>{posts.length + 1}</td>
                <td>
                  <input className="form-control" placeholder="Enter title"
                         required onChange={(e) => this.changePostField('title', e.target.value)}
                         value={newOrEditPost.title} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter body"
                         required onChange={(e) => this.changePostField('body', e.target.value)}
                         value={newOrEditPost.body} />
                </td>
                <td>
                  <input className='form-control'
                         placeholder="Enter image link"
                         required onChange={(e) => this.changePostField('image', e.target.value)}
                         value={newOrEditPost.image} />
                </td>
                <td>
                  <button className='btn btn-outline-success' disabled={savePostDisabled} onClick={this.saveNewPost}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
                      <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                    </svg>
                  </button>
                </td>
                <td>
                  <button className='btn btn-outline-dark' onClick={() => this.clearPostForm()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                      <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            ) : ('')}
          </tbody>
        </table>
        <div className="d-flex flex-row-reverse bd-highlight">
          <button className='btn btn-outline-secondary add' onClick={() => this.addingPost()} disabled={addPost}>+ post</button>
        </div>
      </div>
    )
  }
}

export default Posts;
