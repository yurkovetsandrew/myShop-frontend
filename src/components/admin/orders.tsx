import React from 'react';
import { OrderType, ServerOrderType } from '../../types/OrderType';
import './styles.scss';

interface IOrdersState {
  orders: OrderType[]
}

class Orders extends React.PureComponent<{}, IOrdersState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      orders: []
    }
  }

  componentDidMount() {
    this.fetchOrders()
  }

  fetchOrders() {
    fetch(`http://localhost:3000/api/orders`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then( (response: ServerOrderType[]) => {
      this.setState({
        orders: response.map( serverOrder => {
          return {
            id: serverOrder.id,
            firstName: serverOrder.first_name,
            lastName: serverOrder.last_name,
            phoneNumber: serverOrder.phone_number,
            email: serverOrder.email,
            adress: serverOrder.adress,
            orderTime: serverOrder.order_time,
            solved: serverOrder.solved,
            price: serverOrder.price,
            productInfos: serverOrder.product_infos.map( serverProductInfo => {
              return {
                quantity: serverProductInfo.quantity,
                product: serverProductInfo.product
              }
            })
          }
        })
      })
    })
  }

  solveOrder(order:OrderType) {
    order['solved'] = true
    fetch(`http://localhost:3000/api/orders/${order.id}`, {
      method: "put",
      body: JSON.stringify(order),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(!response.status) {
        this.fetchOrders()
      }
    })
  }

  deleteOrder(orderId:number) {
    fetch(`http://localhost:3000/api/orders/${orderId}`, { method: 'delete' })
      .then(() => {
        this.fetchOrders()
      })
  }


  render() {
    const { orders } = this.state

    return (
      <div className="content">
        <table className='table table-striped table-bordered'>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>First name</th>
              <th scope='col'>Last name</th>
              <th scope='col'>Price</th>
              <th scope='col'>Adress</th>
              <th scope='col'>Phone number</th>
              <th scope='col'>Email</th>
              <th scope='col'>Order ID</th>
              <th scope='col'>Order time</th>
              <th scope='col'>Quantity</th>
              <th scope='col'>Title</th>
              <th scope='col'>Product ID</th>
              <th scope='col'>Solved</th>
              <th scope='col'></th>
            </tr>
          </thead>
          <tbody>
            {orders.map( (order:OrderType, index) => (
              <tr key={index} className={order.solved === false ? 'not-solved' : ''}>
                <td>{index + 1}</td>
                <td>{order.firstName}</td>
                <td>{order.lastName}</td>
                <td>{order.price}</td>
                <td>{order.adress}</td>
                <td>{order.phoneNumber}</td>
                <td>{order.email}</td>
                <td>{order.id}</td>
                <td>{order.orderTime}</td>
                <td>
                  <table>
                    <tbody>
                      {order.productInfos.map( (productsInfo, index) => (
                        <tr key={index}>
                          <td>{productsInfo.quantity}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </td>
                <td>
                  <table>
                    <tbody>
                      {order.productInfos.map( (productsInfo, index) => (
                        <tr key={index}>
                          <td>{productsInfo.product.title}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </td>
                <td>
                  <table>
                    <tbody>
                      {order.productInfos.map( (productsInfo, index) => (
                        <tr key={index}>
                          <td>{productsInfo.product.id}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </td>
                <td>
                  {order.solved === false ? (
                    <button className='btn btn-outline-success' onClick={() => this.solveOrder(order)}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-check" viewBox="0 0 16 16">
                        <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/>
                      </svg>
                    </button>
                  ) : (
                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-check-all" viewBox="0 0 16 16">
                      <path d="M8.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L2.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093L8.95 4.992a.252.252 0 0 1 .02-.022zm-.92 5.14.92.92a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 1 0-1.091-1.028L9.477 9.417l-.485-.486-.943 1.179z"/>
                    </svg>
                  )}
                </td>
                <td>
                  <button className='btn btn-outline-danger' onClick={() => this.deleteOrder(order.id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                      <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                      <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                    </svg>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default Orders;
