import React from 'react';
import Links from './links';
import ProductCard from '../collection/productCard';
import { ProductType } from '../../types/ProductType';
import PostCard from '../blog/post';
import { PostType } from '../../types/PostType';
import './styles.scss';

interface IHomeState {
  products: ProductType[],
  posts: PostType[]
}

class Home extends React.PureComponent<{}, IHomeState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      products: [],
      posts: []
    }
  }

  componentDidMount() {
    let scrollpos:number = window.scrollY
    document.querySelector('.navbar').classList.add('only-on-home-page')
    const scrollChange = 500
    const removeClassOnScroll = () => {
      document.querySelector('.navbar').classList.remove('only-on-home-page')
    }
    const addClassOnScroll = () => {
      document.querySelector('.navbar').classList.add('only-on-home-page')
    }
    window.addEventListener('scroll', () => {
      if(window.location.href === 'http://localhost:3000/') {
        scrollpos = window.scrollY
        if (scrollpos >= scrollChange) { removeClassOnScroll() }
        if (scrollpos <= scrollChange) { addClassOnScroll() }
      } else {
        removeClassOnScroll()
      }
    })
    this.fetchProducts()
    this.fetchPosts()
  }

  fetchProducts() {
    fetch(`http://localhost:3000/api/products?limit=4`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        products: response,
      })
    })
  }

  fetchPosts() {
    fetch(`http://localhost:3000/api/articles?limit=3`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        posts: response,
      })
    })
  }

  render() {
    const { products, posts } = this.state
    console.log(products)
    console.log(posts)

    return (
      <div>
        <div className='image-div home-image d-flex flex-column align-items-center justify-content-center'>
          <h1 className='flex-item'>Disconnect. Relax.</h1>
          <a className='flex-item text-decoration-none' href='/woman'>Shop now</a>
        </div>
        <div className='px-5 py-4 image-description-1'>
          <p className='text-center mb-5'>Picture yourself on the breathtaking shoreline of Lago di Braies in the heart of the Dolomites. A cold morning breeze is sailing in over the frozen lake that shimmers in all nuances from deep dark blue to emerald green. Just as the sun is about to break over the majestic mountain backdrop, you take a long deep breath and close your eyes. This is our brand captured in a moment <b>#Naturemoment</b>.</p>
          <hr/>
        </div>
        <div className='new-arrivals'>
          <h2 className='text-center mb-5 mt-3'>New Arrivals</h2>
          <div className='d-flex justify-content-between'>
            {products.map( (product, index) => (
              <div>
                <ProductCard key = {index} product = {product} />
              </div>
            ))}
          </div>
          <hr/>
        </div>
        <div className='image-div second-image d-flex flex-column align-items-center justify-content-center px-5 mt-5'>
          <h1 className='flex-item mb-5'>100% Europe</h1>
          <p className='flex-item fs-5 text-center'>While most outdoor clothes are made in Asia and shipped for weeks in fossil-fueled cargo ships, Nature store products are all made in Europe from European fabrics, most of which are recycled and sustainable.</p>
        </div>
        <div className='image-description-2 my-5'>
          <h2 className='text-center mb-4'>Buy less, use more.</h2>
          <p className='text-center mx-auto w-50'>Our goal is not to make garments  tailored for specific activities or seasons, we leave that to others. We aim to be awesome regardless of what outdoor endeavors you engage in and when. Buy less, use more.</p>
        </div>
        <div className='last-posts'>
          <h2 className='text-center mb-5 mt-3'>New Posts</h2>
          <div className='d-flex justify-content-between'>
            {posts.map( (post, index) => (
              <div>
                <PostCard key = {index} post = {post} />
              </div>
            ))}
          </div>
        </div>
        <div className='image-div third-image d-flex flex-column align-items-center justify-content-center px-5'>
          <h1 className='flex-item mb-5'>We’re ditching seasons!</h1>
          <p className='flex-item fs-5 text-center'>85% of all textiles goes to the dump each year! At Nature store, we’re ditching seasons, annual collections and new trends and instead focusing on timeless high-quality essentials to be worn a lifetime.</p>
        </div>
        <div className='image-description-2 my-5'>
          <h2 className='text-center mb-4'>#naturestore</h2>
          <p className='text-center mx-auto w-50 small-text'>How do you disconnect & breathe? Please share your stories and pictures on our Instagram. </p>
        </div>
        <Links />
      </div>
    )
  }
}

export default Home;
