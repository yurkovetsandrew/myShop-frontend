import React from 'react';
import Navbar from './navbar';
import ProductPage from './collection/productPage';
import PostPage from './blog/postPage';
import Home from './home';
import Collection from './collection';
import Blog from './blog';
import About from './about';
import Cart from './cart';
import Account from './account';
import Admin from './admin';
import Footer from './footer';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import '../styles.scss';

class AppContent extends React.PureComponent {

  render() {
    return (
      <div>
        <BrowserRouter>
          <Navbar />
          <Switch>
            <Route path='/' exact>
              <Home />
            </Route>
            <Route path='/admin'>
              <Admin />
            </Route>
            <Route path='/woman'>
              <Collection gender='woman' />
            </Route>
            <Route path='/man'>
              <Collection gender='man' />
            </Route>
            <Route path='/product/'>
              <ProductPage  />
            </Route>
            <Route path='/post/'>
              <PostPage  />
            </Route>
            <Route path='/blog'>
              <Blog />
            </Route>
            <Route path='/about'>
              <About />
            </Route>
            <Route path='/cart'>
              <Cart />
            </Route>
            <Route path='/account'>
              <Account />
            </Route>
            <Route path="*">
              <div className='pt-15 mh-832'>
                <h1>404</h1>
                <h3>Page not found</h3>
              </div>
            </Route>
          </Switch>
        </BrowserRouter>
        <Footer />
      </div>
    )
  }
}

export default AppContent;
