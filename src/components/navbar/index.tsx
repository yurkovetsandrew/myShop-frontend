import React from 'react';
import { NavLink } from 'react-router-dom';
import './styles.scss';

class Navbar extends React.PureComponent {

  click(home:boolean) {
    if(home) {
      document.querySelector('.navbar').classList.add('only-on-home-page')
    } else {
      document.querySelector('.navbar').classList.remove('only-on-home-page')
    }
  }

  render() {

    return (
      <nav className='navbar navbar-expand-lg navbar-light position-fixed w-100 pt-2 px-13'>
        <NavLink exact className='nav-link h-60' to='/' onClick={() => this.click(true)}>
          <p className='fw-bold fs-4 mx-2'>NATURE STORE</p>
        </NavLink>
        <div className="collapse navbar-collapse d-flex bd-highlight">
          <div className='navbar-nav me-auto bd-highlight'>
            <NavLink exact className='nav-link h-40' to='/woman' onClick={() => this.click(false)} activeClassName='selected'>
              <p className='fw-bold mx-2'>WOMEN'S</p>
            </NavLink>
            <NavLink exact className='nav-link h-40' to='/man' onClick={() => this.click(false)} activeClassName='selected'>
              <p className='fw-bold mx-2'>MEN'S</p>
            </NavLink>
            <NavLink exact className='nav-link h-40' to='/blog' onClick={() => this.click(false)} activeClassName='selected'>
              <p className='fw-bold mx-2'>BLOG</p>
            </NavLink>
            <NavLink exact className='nav-link h-40' to='/about' onClick={() => this.click(false)} activeClassName='selected'>
              <p className='fw-bold mx-2'>ABOUT US</p>
            </NavLink>
          </div>
          <div className='navbar-nav bd-highlight'>
            <NavLink exact className='nav-link float-right h-40' to='/cart' onClick={() => this.click(false)} activeClassName='selected'>
              <p className='mx-2'>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-bag" viewBox="0 0 16 16">
                  <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                </svg>
              </p>
            </NavLink>
            <NavLink exact className='nav-link float-right h-40' to='/account' onClick={() => this.click(false)} activeClassName='selected'>
              <p className='mx-2'>
                <svg xmlns='http://www.w3.org/2000/svg' width="16" height="16" fill="currentColor" className='bi bi-person' viewBox="0 0 16 16">
                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                </svg>
              </p>
            </NavLink>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
