import React from 'react';
import { PostType } from '../../types/PostType'
import './styles.scss';

interface IPostsState {
  posts: PostType[];
}

class Blog extends React.PureComponent<{}, IPostsState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      posts: []
    }
  }

  componentDidMount() {
    this.fetchPosts()
  }

  fetchPosts() {
    fetch(`/api/articles`, {
      credentials: 'same-origin'
    }).then(response => response.json()).then(response => {
      this.setState({
        posts: response,
      })
    })
  }

  render() {
    return (
      <div>
        <h1>Posts</h1>
        {this.state.posts.map( (post, index) => {
          return (
            <div key={index}>
              <h3>
                {post.title}
              </h3>
              <p>{post.body}</p>
            </div>
          )
        })}
      </div>
    )
  }
}

export default Blog;
