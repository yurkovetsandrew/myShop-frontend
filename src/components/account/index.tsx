import React from 'react';
import './styles.scss';

interface ICollectionState {
  logInDisabled: boolean,
  account: {
    login: string,
    password: string
  }
}

class Account extends React.PureComponent< {}, ICollectionState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      logInDisabled: true,
      account: {
        login: '',
        password: ''
      }
    }
  }

  componentDidMount() {
    if(localStorage.natureAdmin === 'true') {
      window.location.href = 'http://localhost:3000/admin';
    }
  }

  handleClick(field: 'login' | 'password' , value:string) {
    const account:{login: string, password: string} = Object.assign({}, this.state.account)
    account[field] = value
    this.setState({
      account,
      logInDisabled: ['login', 'password'].some( (f: 'login' | 'password') => account[f] === '' ),
    })
  }

  logIn = () => {
    fetch(`http://localhost:3000/admin/password`, {
      method: "post",
      body: JSON.stringify(this.state.account),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(response.success === true) {
        localStorage.setItem('natureAdmin', 'true')
        window.location.href = 'http://localhost:3000/admin';
      }
    })
  }

  render() {
    return (
      <div className='mx-5 mh-832 pt-15'>
        <h1 className='text-center mb-5'>LOG IN</h1>
        <div className='w-50 m-auto'>
          <div className='form-group mb-4'>
            <input className="form-control" placeholder='Enter login'
                 required onChange={(e) => this.handleClick('login', e.target.value)}
                 value={this.state.account.login} />
          </div>
          <div className='form-group mb-4'>
            <input className='form-control' placeholder='Enter password' type='password'
                 required onChange={(e) => this.handleClick('password', e.target.value)}
                 value={this.state.account.password} />
          </div>

          <button className='btn btn-secondary w-100' disabled={this.state.logInDisabled} onClick={this.logIn}>Log in</button>
        </div>
      </div>
    )
  }
}

export default Account
