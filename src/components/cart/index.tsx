import React from 'react';
import { ProductType } from '../../types/ProductType'
import { OrderType, OrderFieldsType } from '../../types/OrderType'
import './styles.scss';

interface ICartState {
  products: ProductType[],
  order: OrderType,
  placeAnOrderDisabled: boolean
}

class Cart extends React.PureComponent<{}, ICartState> {
  constructor(props:{}) {
    super(props);
    this.state = {
      products: [],
      order: {
        firstName: '',
        lastName: '',
        id: null,
        phoneNumber: '',
        email: '',
        adress: '',
        orderTime: '',
        solved: false,
        price: '',
        productInfos: [],
      },
      placeAnOrderDisabled: true,
    }
  }

  componentDidMount() {
    const productsInCart:ProductType[] = JSON.parse(localStorage.getItem('productsInCart'))
    const order:OrderType = this.state.order
    if(productsInCart) {
      order.solved = false
      productsInCart.map( (product) => {
        order.productInfos.push({quantity: 1, product: product})
      })
      this.setState({
        order: order,
        products: productsInCart
      })
    }
    this.totalPrice()
  }

  totalPrice() {
    let totalPrice:number = 0
    console.log(this.state.order)
    this.state.order.productInfos.map( (productInfo) => {
      totalPrice = +productInfo.product.price * productInfo.quantity
    })
    totalPrice += 2
    const order = {...this.state.order}
    order.price = `${totalPrice}`
    this.setState({ order: order })
  }

  changeQuantity(e:string, index:number) {
    let quantity:number = +e
    if(quantity > 0) {
      const order = {...this.state.order}
      order.productInfos[index].quantity = quantity
      this.setState({ order }, this.totalPrice)
    }
  }

  changeField(field: OrderFieldsType | 'orderTime', value:string) {
    const order = {...this.state.order}
    order[field] = value
    this.setState({
      order: order,
      placeAnOrderDisabled: ['firstName', 'lastName', 'phoneNumber', 'email', 'adress'].some( (f: OrderFieldsType) => order[f] === '' ),
    })
  }

  clearCart = () => {
    localStorage.removeItem('productsInCart')
    this.setState({
      products: []
    })
  }

  deleteProduct = (product:ProductType) => {
    let productsInCart:ProductType[] = JSON.parse(localStorage.getItem('productsInCart'))
    productsInCart.map( (productInCart, index) => {
      if(product.id === productInCart.id) {
        productsInCart.splice(index, 1)
      }
    })
    this.setState({
      products: productsInCart
    })
    localStorage.setItem('productsInCart', JSON.stringify(productsInCart))
  }

  placeAnOrder = () => {
    const order = {...this.state.order}
    const data = {
      first_name: order.firstName,
      last_name: order.lastName,
      phone_number: order.phoneNumber,
      email: order.email,
      adress: order.adress,
      order_time: order.orderTime,
      solved: order.solved,
      price: order.price,
      product_infos: order.productInfos.map( serverProductInfo => {
        return {
          quantity: serverProductInfo.quantity,
          product_id: serverProductInfo.product.id
        }
      } )
    }
    fetch(`http://localhost:3000/api/orders`, {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      }
    }).then(response => response.json()).then(response => {
      if(!response.status) {
        localStorage.removeItem('productsInCart')
        this.setState({
          products: [],
          placeAnOrderDisabled: true
        })
      }
    })
  }

  divStyle = (product:ProductType) => {
    return {
      color: 'blue',
      backgroundImage: 'url(' + product.image + ')',
    }
  }

  render() {
    const { order, products, placeAnOrderDisabled } = this.state;

    return (
      <div className='cart pt-15 mh-832'>
        {products.length ? (
          <div className='w-100 d-flex'>
            <div className='cart-item w-75'>
              <div className='d-flex justify-content-between px-4'>
                <h6>MAKING AN ORDER</h6>
                <button className='btn' onClick={this.clearCart}>Clear cart</button>
              </div>
              <div className='jumbotron'>
                <p>By courier to the door - <b>{products.length} product</b></p>
                <p>Fitting before purchase. Delivery time 2-7 days</p>
                <hr className='hr-cart'/>
                {order.productInfos.map((productInfo, index) =>
                  <div className='d-flex flex-row justify-content-between p-3 mb-3 cart-product align-items-center' key={index}>
                    <div className='d-flex'>
                      <a href={`/product/${productInfo.product.id}`}>
                        <div className='product-image-cart' style={this.divStyle(productInfo.product)}></div>
                      </a>
                      <a href={`/product/${productInfo.product.id}`}>
                        <div className='cart-product-info px-3'>
                          <h3>{productInfo.product.title}</h3>
                          <p>{productInfo.product.price} $</p>
                        </div>
                      </a>
                    </div>
                    <div className='d-flex'>
                      <div className='d-flex'>
                        <div className='pt-2'>
                          <p className='mx-2'>Quantity:</p>
                        </div>
                        <input type="number" className="form-control quantity" min="1"
                               value={productInfo.quantity} onChange={(e) => this.changeQuantity(e.target.value, index)}/>
                        <button type="button" className="btn btn-outline-danger mx-3" onClick={() => this.deleteProduct(productInfo.product)}>Delete</button>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className='jumbotron'>
                <form className="row g-3">
                  <div className="col-md-6">
                    <label className="form-label">First Name</label>
                    <input type="text" className="form-control" placeholder="Manuel"
                           onChange={(e) => this.changeField('firstName', e.target.value)}/>
                  </div>
                  <div className="col-md-6">
                    <label className="form-label">Second name</label>
                    <input type="text" className="form-control" placeholder="Bosh"
                           onChange={(e) => this.changeField('lastName', e.target.value)}/>
                  </div>
                  <div className="col-12">
                    <label className="form-label">Address</label>
                    <input type="text" className="form-control" placeholder="Mexico sity, revolution st., 8/23"
                           onChange={(e) => this.changeField('adress', e.target.value)}/>
                  </div>
                  <div className="col-md-6">
                    <label className="form-label">Email</label>
                    <div className="input-group">
                      <div className="input-group-text">@</div>
                      <input type="email" className="form-control" placeholder="email@example.com"
                             onChange={(e) => this.changeField('email', e.target.value)}/>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <label className="form-label">Phone</label>
                    <input type="number" className="form-control" placeholder="+123943821"
                           onChange={(e) => this.changeField('phoneNumber', e.target.value)}/>
                  </div>
                  <div className="col-12">
                    <label className="form-label">Desired delivery date and time</label>
                    <input type="text" className="form-control" placeholder="I will be free tomorrow afternoon"
                           onChange={(e) => this.changeField('orderTime', e.target.value)}/>
                  </div>
                </form>
              </div>
            </div>
            <div className='cart-item w-25'>
              <div className='px-4 mb-3'>
                <h6>MY ORDER</h6>
              </div>
              <div className='jumbotron'>
                <p>By courier to the door</p>
                <hr/>
                <div className='d-flex justify-content-between px-3'>
                  <p>Delivery:</p>
                  <p>2$</p>
                </div>
                <div className='d-flex justify-content-between px-3'>
                  <p>Order amount:</p>
                  <p>{+order.price > 1 ? `${+order.price - 2}` : 0} $</p>
                </div>
                <hr/>
                <div className='d-flex justify-content-between px-3'>
                  <h6>Total order amount:</h6>
                  <p>{order.price ? order.price : 0} $</p>
                </div>
                <button type="button" className="btn btn-outline-secondary w-100" disabled={placeAnOrderDisabled} onClick={this.placeAnOrder}>Place an order</button>
              </div>
            </div>
          </div>
        ) : (
          <div className='d-flex mx-auto my-5 cart-is-empty justify-content-center'>
            <h1>Cart is empty</h1>
          </div>
        )}
      </div>
    )
  }
}

export default Cart;
