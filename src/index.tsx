import React from 'react';
import ReactDOM from 'react-dom';
import AppContent from './components/AppContent';

ReactDOM.render(
  <AppContent />,
  document.querySelector('#app')
);
