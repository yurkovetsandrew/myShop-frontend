const webpack = require( 'webpack' );
const path = require( 'path' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const autoprefixer = require( 'autoprefixer' );
const OptimizeCssAssetsPlugin = require( 'optimize-css-assets-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
const CompressionPlugin = require( 'compression-webpack-plugin' );

const DEV_MODE = process.env.NODE_ENV === 'development';

const plugins = [
  new webpack.DefinePlugin( {
    DEV_MODE,
  } ),
  new HtmlWebpackPlugin( {
    template: './index.html',
    inject: 'body',
  } ),
  new MiniCssExtractPlugin(),
  new OptimizeCssAssetsPlugin()
];

module.exports = {
  entry: {
    main: './src/index.tsx',
  },
  output: {
    publicPath: '/',
    path: path.resolve( __dirname, 'dist' ),
  },
  devServer: {
    historyApiFallback: true,
    disableHostCheck: true,
    hot: false,
    inline: false,
    port: 8131,
  },
  node: {
    fs: 'empty'
  },
  devtool: DEV_MODE && 'source-map',
  resolve: {
    modules: ['node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(tsx|ts)$/,
        exclude: /node_modules/,
        use: 'ts-loader'
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: DEV_MODE ? 'style-loader' : MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: DEV_MODE
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer( {
                  overrideBrowserslist: ["last 2 versions","> 5%", "ie >= 10"]
                } )
              ],
              sourceMap: DEV_MODE
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: DEV_MODE
            }
          }
        ]
      },
      {
        test: /\.(svg|woff|woff2|eot|ttf|otf|csv|pdf|png|jpg)$/,
        use: [
          {
            loader: 'url-loader?limit=100000'
          }
        ]
      }
    ]
  },
  optimization: {
    minimize: !DEV_MODE
  },
  plugins: DEV_MODE ? plugins : [
    ...plugins,
    new CleanWebpackPlugin(),
    new CompressionPlugin( {
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.jsx$|\.ts$|\.tsx$|\.scss$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.9
    } )
  ],
  mode: DEV_MODE ? 'development' : 'production'
};
